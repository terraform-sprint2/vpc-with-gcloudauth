# Deploying VPC, Subnets, Firewall, NAT to GCP using Terraform




## Deployment

To deploy this project run

```bash
  gcloud init
  gcloud auth application-default login
  terraform init
  terraform plan
  terraform apply
```



## Screenshot - VPC with private and public subnets

![VPC Screenshot](screenshots/VPC-home.png)
![VPC Screenshot-2](screenshots/VPC-detail.png)

## Screenshot - Firewall

![Firewall Screenshot](screenshots/firewall.png)

## Screenshot - VM instances

![VM Screenshot](screenshots/VM-instances.png)

## Screenshot - Cloud NAT

![NAT Screenshot](screenshots/nat.png)
![NAT Screenshot-2](screenshots/nat-detail.png)

## Screenshot - Cloud Route

![Route Screenshot](screenshots/route.png)
![Route Screenshot-2](screenshots/route-detail.png)

# Screenshot - Deploying apache webserver on VM

![Apache Screenshot](screenshots/apache.png)