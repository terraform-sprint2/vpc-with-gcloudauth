//Provider
provider "google" {
    project = "${var.project_name}"
    region = "${var.region}"
}

//Creating a VPC network
resource "google_compute_network" "vpc" {
    name = "${var.name}-vpc"
    auto_create_subnetworks = false
}

//Creating a public subnet
resource "google_compute_subnetwork" "public" {
    name = "${var.name}-public-subnet"
    ip_cidr_range = "${var.cidr}"
    region = "${var.region}"
    network = "${var.name}-vpc"
    depends_on = [
      "google_compute_network.vpc"
    ]
    private_ip_google_access = false
}

//Creating a private subnet
resource "google_compute_subnetwork" "private" {
    name = "${var.name}-private-subnet"
    ip_cidr_range = "${var.prcidr}"
    region = "${var.region}"
    network = "${var.name}-vpc"
    depends_on = [
      "google_compute_network.vpc"
    ]
    private_ip_google_access = true
}

//Create a firewall to allow ssh, http and https
resource "google_compute_firewall" "firewall" {
    name = "${var.name}-firewall"
    network = google_compute_network.vpc.name
    allow {
      protocol = "icmp"
    }
    allow {
      protocol = "tcp"
      ports = ["22","80","443"]
    }

    source_ranges = ["0.0.0.0/0"]
}

//Creating a virtual machine on public subnet
resource "google_compute_instance" "default" {
    name = "${var.name}-public-vm"
    machine_type = "${var.machinetype}"
    zone = "${var.zone}"
    boot_disk {
      initialize_params {
          image = "${var.image}"
      }
    }
    network_interface {
      network = google_compute_network.vpc.id
      subnetwork = google_compute_subnetwork.public.name
      access_config {
        
      }

    }
    tags = ["http-server","https-server"]
    metadata_startup_script = "sudo apt update && sudo apt -y install apache2"
}

//Creating a virtual machine on private subnet
resource "google_compute_instance" "private-vm" {
    name = "${var.name}-privat-vm"
    machine_type = "${var.machinetype}"
    zone = "${var.zone}"
    boot_disk {
      initialize_params {
          image = "${var.image}"
      }
    }
    network_interface {
      network = google_compute_network.vpc.id
      subnetwork = google_compute_subnetwork.private.name
    }
    tags = ["http-server","https-server"]
    metadata_startup_script = "sudo apt update && sudo apt -y install mysql-server && sudo systemctl start mysql.service"
}

//Creating a router for NAT gateway
resource "google_compute_router" "router" {
    name = "${var.name}-route"
    network = google_compute_network.vpc.name
    region = "${var.region}"
    bgp {
    asn = 64514
  }
}

//Creating NAT gateway
resource "google_compute_router_nat" "nat" {
    name = "${var.name}-nat"
    router = google_compute_router.router.name
    region = google_compute_router.router.region
    nat_ip_allocate_option = "AUTO_ONLY"
    source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

    subnetwork {
      name = google_compute_subnetwork.private.id
      source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
    }
}
